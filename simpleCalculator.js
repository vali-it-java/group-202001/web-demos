
function add() {
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    let sum = parseInt(number1.value) + parseInt(number2.value);

    let result = document.querySelector("#result");
    result.innerHTML = sum;
}

function sub() {
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    let sum = parseInt(number1.value) - parseInt(number2.value);

    let result = document.querySelector("#result");
    result.innerHTML = sum;
}

function mult() {
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    let sum = parseInt(number1.value) * parseInt(number2.value);

    let result = document.querySelector("#result");
    result.innerHTML = sum;
}

function div() {
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    let sum = parseInt(number1.value) / parseInt(number2.value);

    let result = document.querySelector("#result");
    result.innerHTML = sum;
}

function clearCalc() {
    let number1 = document.querySelector('#number1');
    let number2 = document.querySelector('#number2');
    number1.value = "";
    number2.value = "";

    let result = document.querySelector("#result");
    result.innerHTML = "";
}